// Récupération du bouton par son type
const ourButton = document.querySelector('input[type="submit"]');

// Récupération de la zone de saisie par son id
const priceInput = document.querySelector('#price');

// Tirage au sort d'un nombre entre 0 et 1000
const rightPrice = Math.floor(Math.random() * 1001);
// console.log(rightPrice);

// Fonction pour afficher les indications au joueur
function printMessage(message) {
	document.getElementById('p1').innerHTML = message;
}

let nEssais = 0;

function rightPriceFunction() {
	let aGuess = priceInput.value;

	nEssais++;

	if (aGuess == rightPrice) {
		printMessage('<b>Félicitations !</b><br>Le juste prix était bien ' + aGuess + '. Trouvé en '+ nEssais + ' coups !');
	}
	else if (aGuess < rightPrice) {
		printMessage('<b>C\'est plus !</b>');
	}
	else {
		printMessage('<b>C\'est moins !</b>');
	}
};

// Quand le bouton est cliqué, on appelle notre fonction
ourButton.addEventListener('click', rightPriceFunction);
