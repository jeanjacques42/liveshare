var testID = prompt("ID du test");

let idOK = false;
let needNumMsg = "Merci de préciser un nombre";

var listID = 
[
    "2a1:Donner la somme de n premiers entiers (n donné en entrée)",
    "2a2:Donner la table de multiplication d’un nombre donné entre 1 et 9",
    "2a3:Donner le plus grand d’une suite de 20 nombres tirés aléatoirement",
    "2a4:Deviner un nombre caché (tiré aléatoirement)",
    "2a5:Affichage de figures géométriques avec le caractère #",
    "marianne1:Test d'algo de Marianne",
    "tab1:Remplir un tableau de nombres tirés au hasard",
    "tab2:Afficher le contenu d’un tableau dans la console, les éléments sont séparés par des virgules",
    "tab3:Rechercher le nombre minimum/maximum dans le tableau",
    "tab4:Tri maison : inventer un algo perso pour trier le contenu du tableau",
    "tab5:Recherche dans un tableau trié",
    "henni:Donne une replique de mohamed henni",
    "conway:Affiche une suite de conway",
    "help:Affiche la liste des exo disponible"
];

for(let i = 1; i < 2; i)
{
    checkID(testID);

    if(idOK == true)
    {
        break;
    }
    else
    {
        testID = prompt("ID du test (!)");
    }
}

function forceNumber(title, minimum, maximum)
{
    let lastRet = "no";
    idOK = null;

    for(let i = 1; i < 2; i)
    {
        let n = prompt(getRetByID(title, lastRet));
        
        lastRet = checkForceNumber(n, minimum, maximum);

        if(idOK.startsWith("ok:"))
        {
            idOK = idOK.split(":")[1];
            break;
        }
        else
        {
            lastRet = checkForceNumber(n, minimum, maximum);
        }
    }

    return idOK;
}

function getRetByID(title, ret)
{
    let error = ret;

    let end = "";
    if(error == "no")
    {
        end = "";
    }
    else if(error == "nan")
    {
        end = " (!) -> Pas un nombre"
    }
    else if(error.startsWith("short"))
    {
        end = " (!) -> Trop petit <" + error.split("_")[1] + ">"
    }
    else if(error.startsWith("long"))
    {
        end = " (!) -> Trop grand <" + error.split("_")[1] + ">"
    }
    else if(error == "null")
    {
        end = " (!) -> Ne peut pas être null"
    }

    return title + end;
}

function checkForceNumber(input, minimum, maximum)
{
    if(!isNaN(input))
    {
        intput = parseInt(input);
        minimum = parseInt(minimum);
        maximum = parseInt(maximum);
    }

    if(isNaN(input))
    {
        idOK = "nan";
    }
    else if(input < minimum)
    {
        idOK = "short_" + minimum;
    }
    else if(input > maximum)
    {
        idOK = "long_" + maximum;
    }
    else if(input == null)
    {
        idOK = "null";
    }
    else
    {
        idOK = "ok:" + input;
    }

    return idOK;
}

if(testID == "help")
{
    console.log("--------------");
    listID.forEach(f =>
    {
        console.log(f.split(":")[0] + " -> " + f.split(":")[1]);
    });
    console.log("--------------");
}

if(testID == "2a1")
{
    let n = forceNumber("Merci de préciser le nombre", 0, 100);
    
    for(let i = 0; i <= n; i++)
    {
        console.log(i);
    }
}

if(testID == "2a2")
{
    let n = forceNumber("Merci de préciser le nombre", 0, 9);

    for(let i = 0; i <= 9; i++)
    {
        console.log(n + "*" + i + " = " + n*i);
    }
}

if(testID == "2a3")
{
    console.log("Random: ");
    let allnum = ";";
    for(let i = 0; i <= 20; i++)
    {
        let num = random(1, 10000);
        allnum = allnum + num + ";";
        console.log("> " + num);
    }

    let bestnum = 0;
    allnum.split(";").forEach(nm =>{
        if(nm.length > 0)
        {
            if(nm > bestnum)
            {
                bestnum = nm;
            }
        }
    });

    console.log("\nMeilleur num: " + bestnum);
}

if(testID == "2a4")
{
    let n = forceNumber("Choisir le nombre maximum", 0, 1000000000);

    let num = random(1, n);
    let num2 = n;

    let guess = 0;
    let trys = 0;

    while(true)
    {
        guess = forceNumber("Devinez le bon nombre", 0, num2);
        trys++;
        if(guess > num)
        {
            console.log("C'est moins !")
        }
        else if(guess < num)
        {
            console.log("C'est plus !")
        }
        else
        {
            console.log("\n-----\nTrouvé !!\nLa réponse était: " + num + " !\nVous avez trouvé en " + trys + " essais !\n-----");
            break;
        }
        console.log("--");
    }
}

if(testID == "2a5")
{
    let forme = forceNumber("Quelle forme faire ? (1-5)", 1, 5);
    let same = forceNumber("Renvoyer le resultat sur la même ligne ? (0-1)", 0, 1);

    let sam;
    if(same == 0)
    {
        sam = false;
    }
    else
    {
        sam = true;
    }

    if(forme == 1)
    {
        let sz = forceNumber("Choisir la taille ? (3-20)", 3, 20);
        let rett = "";

        for(let i = 1; i <= sz; i++)
        {
            let ret = "";
            let tagL = i;
            let spacL = sz-i
            let charL = sz;
            while(charL > 0)
            {
                if(tagL > 0)
                {
                    ret = ret + "#";
                    tagL--;
                }
                else if(spacL > 0)
                {
                    ret = ret + " ";
                    spacL--;
                }
                charL--;
            }
            if(sam == false)
            {
                console.log(ret);
            }
            else
            {
                rett = rett + ret + "\n";
            }
        }
        if(sam == true)
        {
            console.log(rett);
        }
    }
    else if(forme == 2)
    {
        let sz = forceNumber("Choisir la taille ? (3-20)", 3, 20);
        let rett = "";

        for(let i = 1; i <= sz; i++)
        {
            let ret = "";
            let tagL = i;
            let spacL = sz-i
            let charL = sz;
            while(charL > 0)
            {
                if(spacL > 0)
                {
                    ret = ret + " ";
                    spacL--;
                }
                else if(tagL > 0)
                {
                    ret = ret + "#";
                    tagL--;
                }
                charL--;
            }
            if(sam == false)
            {
                console.log(ret);
            }
            else
            {
                rett = rett + ret + "\n";
            }
        }
        if(sam == true)
        {
            console.log(rett);
        }
    }
    else if(forme == 3)
    {
        let sz1 = forceNumber("Choisir la longueur ? (3-50)", 3, 50);
        let sz2 = forceNumber("Choisir la hauteur ? (3-50)", 3, 50);
        let rett = "";

        for(let i = 1; i <= sz2; i++)
        {
            let ret = "";
            let lenght = sz1;

            if(i == 1 || i == sz2)
            {
                while(lenght > 0)
                {
                    ret = ret + "#";
                    lenght--;
                }
            }
            else
            {
                while(lenght > 0)
                {
                    if(lenght == 1 || lenght == sz1)
                    {
                        ret = ret + "#";
                    }
                    else
                    {
                        ret = ret + " ";
                    }
                    lenght--;
                }
            }
            if(sam == false)
            {
                console.log(ret);
            }
            else
            {
                rett = rett + ret + "\n";
            }
        }
        if(sam == true)
        {
            console.log(rett);
        }
    }
    else if(forme == 4)
    {
        let sz = forceNumber("Choisir la taille ? (3-20)", 3, 20);
        let rett = "";

        for(let i = sz; i > 0; i--)
        {
            let ret = "";
            let tagL = i;
            let spacL = sz-i
            let charL = sz;
            while(charL > 0)
            {
                if(tagL > 0)
                {
                    ret = ret + "#";
                    tagL--;
                }
                else if(spacL > 0)
                {
                    ret = ret + " ";
                    spacL--;
                }
                charL--;
            }
            if(sam == false)
            {
                console.log(ret);
            }
            else
            {
                rett = rett + ret + "\n";
            }
        }
        if(sam == true)
        {
            console.log(rett);
        }
    }
    else
    {
        let sz = forceNumber("Choisir la taille ? (3-20)", 3, 20);
        let rett = "";

        for(let i = 0; i < sz; i++)
        {
            let ret = "";
            let tagL = sz-i;
            let spacL = i;
            let charL = sz;
            while(charL > 0)
            {
                if(spacL > 0)
                {
                    ret = ret + " ";
                    spacL--;
                }
                else if(tagL > 0)
                {
                    ret = ret + "#";
                    tagL--;
                }
                charL--;
            }
            if(sam == false)
            {
                console.log(ret);
            }
            else
            {
                rett = rett + ret + "\n";
            }
        }
        if(sam == true)
        {
            console.log(rett);
        }
    }
}

if(testID == "marianne1")
{
    let n = forceNumber("Merci de préciser le nombre", 0, 20);
    let velocity = forceNumber("Distance d'affichage (1-20)", 1, 20);
    let same = forceNumber("Renvoyer le resultat sur la même ligne ? (0-1)", 0, 1);

    velocity = parseInt(velocity);

    let sam;
    if(same == 0)
    {
        sam = false;
    }
    else
    {
        sam = true;
    }
    
    let acspace = 0;
    let rett = "";

    let lineLeft = n;
    let velo = lineLeft;

    while(lineLeft > 0)
    {
        let ret = "";
        acspace = parseInt(acspace);
        lineLeft = parseInt(lineLeft);

        velo = velo*((velocity+75)/100);
        acspace = Math.round(acspace+velo);
        let spaceleft = acspace;
        while(spaceleft > 0)
        {
            ret = ret + " ";
            spaceleft--;
        }
        ret = ret + "#";
        lineLeft--;
        if(sam == false)
        {
            console.log(ret);
        }
        else
        {
            rett = rett + ret + "\n";
        }
    }
    if(sam == true)
    {
        console.log(rett);
    }

}

if(testID == "tab1")
{
    let n = forceNumber("Merci de préciser la taille du tableau", 0, 100);
    let tab = randomTab(n, 1000, true);

    console.log("Le tableau contient: " + tab.length + " valeures.");
}

if(testID == "tab2")
{
    let n = forceNumber("Merci de préciser la taille du tableau", 0, 1000);
    let tab = randomTab(n, 1000, true);

    let ret = "";
    let left = tab.length;
    while(left > 0)
    {
        left--;
        ret = ret + tab[left] + ",";
    }

    console.log(ret.substring(0, ret.length-1));
}

if(testID == "tab3")
{
    let n = forceNumber("Merci de préciser la taille du tableau", 0, 1000);
    let max = forceNumber("Merci de préciser la valeur maximale", 0, 1000000);
    let tab = randomTab(n, max, true);

    let most = 0;
    let less = max;

    let ret = "";
    let left = tab.length;
    while(left > 0)
    {
        left--;
        ret = ret + tab[left] + ",";
    }

    tab.forEach(v =>{
        if(v > most)
        {
            most = v;
        }
        if(v < less)
        {
            less = v;
        }
    })

    console.log(ret.substring(0, ret.length-1) + "\n-----------\n");
    console.log("Plus petite valeur: " + less + "\nPlus grande valeur: " + most);
}

if(testID == "tab4")
{
    let n = forceNumber("Merci de préciser la taille du tableau", 0, 1000);
    let max = forceNumber("Merci de préciser la valeur maximale", 0, 1000000);
    let d = forceNumber("Autoriser les valeur en double (0-1)", 0, 1);

    let db;
    if(d == 0)
    {
        db = false;
    }
    else
    {
        db = true;
    }

    let tab = randomTab(n, max, db);

    let ret = "";
    let left = tab.length;
    while(left > 0)
    {
        left--;
        ret = ret + tab[left] + ",";
    }

    console.log(ret.substring(0, ret.length-1) + "\n-----------\n");
    
    let ret2 = "";
    let less = max;

    let clLeft = tab.length;
    while(clLeft > 0)
    {
        tab.forEach(v =>{
            if(v < less)
            {
                less = v;
            }
        })

        let index = tab.indexOf(less);
        if(index > -1)
        {
            tab.splice(index, 1);
        }
        ret2 = ret2 + less + ",";
        less = max;
        clLeft--;
    }

    console.log(ret2.substring(0, ret2.length-1) + "\n-----------\nNombre restants: " + tab.length);
}

if(testID == "tab5")
{
    let n = forceNumber("Merci de préciser la taille du tableau", 0, 1000);
    let max = forceNumber("Merci de préciser la valeur maximale", 0, 1000000);
    let tab = randomTab(n, max);

    let ret = "";
    let left = tab.length;
    while(left > 0)
    {
        left--;
        ret = ret + tab[left] + ",";
    }

    console.log(ret.substring(0, ret.length-1) + "\n-----------\n");

    for(let i = 0; i < 2; i)
    {
        let search = forceNumber("Rechercher un nombre", -1, max);
        if(search == -1)
        {
            break;
        }
        search = parseInt(search);
        let index = tab.indexOf(search);
        if(index > -1)
        {
            console.log("Le nombre " + search + " à été trouvé à l'emplacement: #" + index);
        }
        else
        {
            console.log("Aucun nombre " + search + " n'a été trouvé");
        }
    }
}

let henniList = 
[
    "chapeaux:regardent el chapo",
    "ruviers:sont qualifiées",
    "coqs sportifs:mettent des préservatifs",
    "bouteilles:sont dans les embouteillages",
    "placards:sont sur bla-bla-car",
    "feux d'artifice:ont un fils",
    "parebrises:se font la bise",
    "coqs:sniffent de la coke",
    "jus de groseille:font de l'oseille",
    "ticket:se font critiquer",
    "mexicaines:se font ken",
    "noires:se noie",
    "cadeau:font du judo",
    "fauteil:sont abonné a oteil",
    "valises:dévalisent",
    "microondes:joue la coupe du monde",
    "portables:porte des tables",
    "redbull:sont nul",
    "otruches:joue en autriche",
    "benzema:baisent emma",
    "bateau:se battent tôt",
    "allemandes:se font mettres a l'amande",
    "pommes d'adan:qui font le ramadan",
    "canettes:sont pas nettes",
    "cartables:petent un cable",
    "chemises:misent",
    "netflix:ont des aides fixes",
    "brocolis:livres des gros colis",
    "meringuées:gays",
    "jerery:touchent le RMI",
    "grosses:jouent a mario bros",
    "portes menteaux:portent des motos",
    "battent:regardent batman",
    "ratées:font du karaté",
    "anniverssaires:sert",
    "parapluies:mettent des bottes de pluie",
    "pommes:tombent dans les pommes",
    "tabourets:sont bourrées",
    "tortues:torturent",
    "boules:vont a istamboul",
    "crapeaux:crapotent",
    "trombonnes:sont trop bonnes",
    "stylo:sont stylés",
    "pupitres:jouent le titre",
    "gros doight:boivent de la badoit",
    "boules:ont les boules",
    "bouteilles de wisky:font de sky",
    "pulles:nous manipullent",
    "crampons:font des p'tit ponts"
]

if(testID == "henni")
{
    let replique = henniList[random(0,henniList.length-1)].split(":");
    console.log("Bonsoir à tous les amis, pour ceux qui m'connaissent pas: vos grand mères les " + replique[0] + ", qui " + replique[1]);
}

if(testID == "conway")
{
    let n = forceNumber("Nombre de ligne à afficher", 2, 20);
    let ret = forceNumber("Nombre de départ à afficher", 1, 10000);
    let dbg = forceNumber("Afficher le debug ? (0-1)", 0, 1);

    let rnn = n-1;
    console.log(ret);
    
    while(n > 0)
    {
        let lastNum;
        let numStreak = 1;

        let debug;
        if(dbg == 0)
        {
            debug = false;
        }
        else
        {
            debug = true;
        }

        let chr = ret.split('');
        let nums = [];

        chr.forEach(c =>{
            if(debug == true)
            {
                console.log("Au tour de " + c);
            }
            if(lastNum == undefined)
            {   
                if(debug == true)
                {
                    console.log(c + ": Premier de la liste (1) -----------");
                }
                lastNum = c;
                numStreak = 1;
            }
            else
            {   
                if(debug == true)
                {
                    console.log("Comparaison entre " + lastNum + " et " + c);
                }
                if(lastNum == c)
                {
                    numStreak++;
                    if(debug == true)
                    {
                        console.log(c + ": Ajouté a la chaine (" + numStreak + ") -----------");
                    }
                }
                else
                {   
                    if(debug == true)
                    {
                        console.log(c + ": Nouveau chiffre");
                    }
                    nums.push([lastNum,numStreak]);
                    lastNum = c;
                    numStreak = 1;
                }
            }
        })
        
        if(debug == true)
        {
            console.log("Dernier chiffre traité");
        }
        nums.push([lastNum,numStreak]);
        lastNum = undefined;
        numStreak = 0;

        ret = "";
        nums.forEach(nm =>{
            ret = ret + nm[1] + nm[0];
        })

        console.log(ret);

        n--;
    }
}

function randomTab(lenght, size, double)
{
    let tab = [];

    for(let i = 0; i < lenght; i)
    {
        let trys = 0;
        let randomNum = random(1, size);
        if((!tab.includes(randomNum)) || double == true)
        {
            tab.push(randomNum);
            i++;
        }
        trys++;
        if(trys > 500)
        {
            break;
        }
    }

    return tab;
}

function checkID(id)
{
    if(testContain(listID, id) == true)
    {
        idOK = true;
    }
    else
    {
        idOK = false;
    }
}

function random(min, max)
{
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function testContain(list, input)
{
    let founded = false;
    list.forEach(f =>
    {
        if(f.split(":")[0] == input)
        {
            founded = true;
        }
    });
    return founded;
}